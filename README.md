# The Official _Savage Worlds_ Game System for Foundry Virtual Tabletop

Published by [Pinnacle Entertainment Group, Inc.](https://peginc.com)

## We Moved!

Hi! If you've come here you were likely looking for the _Savage Worlds_ game system for Foundry VTT. We've moved the system to the new repository which you can find at this address: [https://gitlab.com/peginc/swade](https://gitlab.com/peginc/swade). 

This repository here has been archived and is no longer in use, so please use the new one.

![Savage Worlds Adventure Edition for Foundry Virtual Tabletop](https://gitlab.com/peginc/savage-worlds-adventure-edition/-/raw/master/logos/SWADE_FVTT.png)

This game system supports and enhances the play experience of _Savage Worlds Adventure Edition_ in [Foundry VTT](https://foundryvtt.com/).

![Chat cards in action](https://gitlab.com/peginc/savage-worlds-adventure-edition/-/raw/master/images/chat-cards.gif)

Designed to feel at home among the pages of the core rules, the character sheet offers a refreshed design, UI improvements, and additional features to streamline and improve your game.

![New Character Sheet Design](https://gitlab.com/peginc/savage-worlds-adventure-edition/-/raw/master/images/new-sheet-design.gif)

Those who use the popular [_Dice So Nice!_ module](https://foundryvtt.com/packages/dice-so-nice/) will see some additional “benefits” as well.

![Spending a Benny](https://gitlab.com/peginc/savage-worlds-adventure-edition/-/raw/master/images/benny.gif)

Full documentation on how to use the system and sheets as well as a collection of reference items for Hindrances, Edges, and Skills are included in the accompanying compendium.

## Coming Soon!
A compendium of core rules–including gear, powers, and bestiary with complete descriptions–will be available for purchase from Pinnacle Entertainment Group, Inc.

**Any time. Any place. _Savage Worlds_.**
