In order to submit an effective bug report, please include the following information along with your issue description.

### Environment Details

> Please share the following basic details about your setup.

- Platform: [Roll20, Foundry VTT, Astral Tabletop, etc...]
- Operating System: [Windows, MacOS, Linux (which distro)]

### Issue Description

> Please provide a description of your issue below. Including console error messages, screenshots or videos can be particularly helpful.

### Relevant logs and/or screenshots

> If you have them, please provide relevant logs or screenshots

### Possible Solution and/or Root Cause

> If you have any possible leads to the Root Cause or suggestions for fixes then please provide them here

/label ~bug
